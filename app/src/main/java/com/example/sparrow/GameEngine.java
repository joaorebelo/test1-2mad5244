package com.example.sparrow;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Random;

public class GameEngine extends SurfaceView implements Runnable {
    private final String TAG = "SPARROW";

    // game thread variables
    private Thread gameThread = null;
    private volatile boolean gameIsRunning;

    // drawing variables
    private Canvas canvas;
    private Paint paintbrush;
    private SurfaceHolder holder;

    // Screen resolution varaibles
    private int screenWidth;
    private int screenHeight;

    // VISIBLE GAME PLAY AREA
    // These variables are set in the constructor
    int VISIBLE_LEFT;
    int VISIBLE_TOP;
    int VISIBLE_RIGHT;
    int VISIBLE_BOTTOM;

    // SPRITES
    Square bullet;
    int SQUARE_WIDTH = 100;

    Square enemy;

    Sprite player;
    Sprite sparrow;
    Sprite cat;
    Sprite cage;

    ArrayList<Square> bullets = new ArrayList<Square>();

    // GAME STATS
    int score = 0;
    int sparrowDelay = 5;
    boolean isfalling = false;
    boolean hasCat = false;
    boolean hasSparrow = false;
    String status = "";
    Square btnRestart = new Square(this.getContext(), this.screenWidth/2, (screenHeight/2+50), 300);

    public GameEngine(Context context, int screenW, int screenH) {
        super(context);

        // intialize the drawing variables
        this.holder = this.getHolder();
        this.paintbrush = new Paint();

        // set screen height and width
        this.screenWidth = screenW;
        this.screenHeight = screenH;

        // setup visible game play area variables
        this.VISIBLE_LEFT = 20;
        this.VISIBLE_TOP = 10;
        this.VISIBLE_RIGHT = this.screenWidth - 20;
        this.VISIBLE_BOTTOM = (int) (this.screenHeight * 0.8);


        // initalize sprites
        this.player = new Sprite(this.getContext(), 100, 700, R.drawable.player64);
        this.sparrow = new Sprite(this.getContext(), 500, 200, R.drawable.bird64);
        this.cat = new Sprite(this.getContext(), 1500, 700, R.drawable.cat64);
        this.cage = new Sprite(this.getContext(), 1500, 50, R.mipmap.ic_cage);

        //this.bullet = new Square(context, 100, 700, SQUARE_WIDTH);

    }

    @Override
    public void run() {
        while (gameIsRunning == true) {
            updateGame();    // updating positions of stuff
            redrawSprites(); // drawing the stuff
            controlFPS();
        }
    }

    boolean cageMoving = true;
    boolean catMoving = true;
    // Game Loop methods
    public void updateGame() {

        if(status.equals("")){
            /////////////////////////////////////
            if(!isfalling){
                //cage movement horizontal
                if (cageMoving == true) {
                    this.cage.setxPosition(this.cage.getxPosition() + 30);
                }
                else {
                    this.cage.setxPosition(this.cage.getxPosition() - 30);
                }
                // update the cage hitbox
                this.cage.updateHitbox();
                // colliding with right side of screen
                if (this.cage.getxPosition() >= VISIBLE_RIGHT-150) {
                    cageMoving = false;
                }
                // colliding with left side of screen
                if (this.cage.getxPosition() < VISIBLE_LEFT ) {
                    cageMoving = true;
                }
            }else {
                //cage movement vertical
                if (cageMoving == true) {
                    this.cage.setyPosition(this.cage.getyPosition() + 30);
                }
                else {
                    this.cage.setyPosition(this.cage.getyPosition() - 30);
                }
                // update the cage hitbox
                this.cage.updateHitbox();
                // colliding with right side of screen
                if (this.cage.getyPosition() >= VISIBLE_TOP-150) {
                    cageMoving = false;
                }
                // colliding with left side of screen
                if (this.cage.getyPosition() < VISIBLE_BOTTOM ) {
                    cageMoving = true;
                }


                //has the cat
                if (this.cage.getHitbox().intersect(this.cat.getHitbox())){hasCat = true;}

                //has the sparrow
                if (this.cage.getHitbox().intersect(this.sparrow.getHitbox())){hasSparrow = true;}

                if (this.cage.getyPosition() >= VISIBLE_BOTTOM){
                    isfalling = false;
                    if (hasCat && !hasSparrow){
                        //win
                        status = "win";

                    }else{
                        //lost
                        status = "lose";
                    }
                }
            }

            /////////////////////////////////////

            //cat movement
            if (catMoving == true) {
                this.cat.setxPosition(this.cat.getxPosition() + 40);
            }
            else {
                this.cat.setxPosition(this.cat.getxPosition() - 40);
            }
            // update the cage hitbox
            this.cat.updateHitbox();
            // colliding with right side of screen
            if (this.cat.getxPosition() >= VISIBLE_RIGHT-220) {
                catMoving = false;
            }
            // colliding with left side of screen
            if (this.cat.getxPosition() < VISIBLE_LEFT+30 ) {
                catMoving = true;
            }

            //bird movement

            boolean notGotNewCoods = true;
            Random r = new Random();

            if (sparrowDelay == 0){
                while (notGotNewCoods){
                    int newX = r.nextInt((screenWidth-200) - (screenWidth/2)) + (screenWidth/2);
                    int newY = r.nextInt((screenHeight-50) - (0)) + (0);
                    sparrow.setxPosition(newX);
                    sparrow.setyPosition(newY);


                    // RESTART THE HITBOX
                    sparrow.updateHitbox();
                    notGotNewCoods = false;
                    sparrowDelay=5;
                }
            }else {
                sparrowDelay--;
            }


            //bullet movement
            for (int i= 0 ; i < bullets.size(); i++){
                int newX = this.bullets.get(i).getxPosition() + (int) (this.bullets.get(i).xn * 30);
                int newY = this.bullets.get(i).getyPosition() + (int) (this.bullets.get(i).yn * 30);
                this.bullets.get(i).setxPosition(newX);
                this.bullets.get(i).setyPosition(newY);
                this.bullets.get(i).updateHitbox();
                if (newX == 0 || newY >= screenWidth){
                    //remove from the game
                }
                if (this.bullets.get(i).getHitbox().intersect(this.cage.getHitbox())){
                    isfalling = true;
                    //Log.d("test", "-----" + isfalling);
                }
            }
        }



    }


    public void outputVisibleArea() {
        Log.d(TAG, "DEBUG: The visible area of the screen is:");
        Log.d(TAG, "DEBUG: Maximum w,h = " + this.screenWidth +  "," + this.screenHeight);
        Log.d(TAG, "DEBUG: Visible w,h =" + VISIBLE_RIGHT + "," + VISIBLE_BOTTOM);
        Log.d(TAG, "-------------------------------------");
    }



    public void redrawSprites() {
        if (holder.getSurface().isValid()) {

            // initialize the canvas
            canvas = holder.lockCanvas();
            // --------------------------------

            // set the game's background color
            canvas.drawColor(Color.argb(255,255,255,255));

            // setup stroke style and width
            paintbrush.setStyle(Paint.Style.FILL);
            paintbrush.setStrokeWidth(8);

            // --------------------------------------------------------
            // draw boundaries of the visible space of app
            // --------------------------------------------------------
            paintbrush.setStyle(Paint.Style.STROKE);
            paintbrush.setColor(Color.argb(255, 0, 128, 0));

            canvas.drawRect(VISIBLE_LEFT, VISIBLE_TOP, VISIBLE_RIGHT, VISIBLE_BOTTOM, paintbrush);
            this.outputVisibleArea();

            // --------------------------------------------------------
            // draw player and sparrow
            // --------------------------------------------------------

            // 1. player
            canvas.drawBitmap(this.player.getImage(), this.player.getxPosition(), this.player.getyPosition(), paintbrush);

            // 2. sparrow
            canvas.drawBitmap(this.sparrow.getImage(), this.sparrow.getxPosition(), this.sparrow.getyPosition(), paintbrush);

            // 3. cat
            canvas.drawBitmap(this.cat.getImage(), this.cat.getxPosition(), this.cat.getyPosition(), paintbrush);

            // 4. cage
            canvas.drawBitmap(this.cage.getImage(), this.cage.getxPosition(), this.cage.getyPosition(), paintbrush);

            // DRAW ALL THE BULLETS
            for (int i = 0; i < this.bullets.size(); i++) {
                // 1. get the (x,y) of the bullet
                Square b = this.bullets.get(i);

                int x = b.getxPosition();
                int y = b.getyPosition();

                // 2. draw the bullet
                paintbrush.setColor(Color.BLACK);
                paintbrush.setStyle(Paint.Style.FILL);
                canvas.drawRect(
                        x,
                        y,
                        x+b.getWidth(),
                        y+b.getWidth(),
                        paintbrush
                );

                // 3. draw the bullet's hitbox
                paintbrush.setColor(Color.GREEN);
                paintbrush.setStyle(Paint.Style.STROKE);
                canvas.drawRect(
                        b.getHitbox(),
                        paintbrush
                );

            }

            // --------------------------------------------------------
            // draw hitbox on player
            // --------------------------------------------------------
            Rect r = player.getHitbox();
            paintbrush.setStyle(Paint.Style.STROKE);
            canvas.drawRect(r, paintbrush);


            // --------------------------------------------------------
            // draw hitbox on player
            // --------------------------------------------------------
            paintbrush.setTextSize(60);
            paintbrush.setStrokeWidth(5);
            String screenInfo = "Screen size: (" + this.screenWidth + "," + this.screenHeight + ")";
            canvas.drawText(screenInfo, 10, 100, paintbrush);

            if (status.equals("win")){
                paintbrush.setTextSize(60);
                paintbrush.setStrokeWidth(5);
                String screenInfo2 = "YOU WIN";
                canvas.drawText(screenInfo2, this.screenWidth/2, screenHeight/2, paintbrush);



            }else if(status.equals("lose")){
                paintbrush.setTextSize(60);
                paintbrush.setStrokeWidth(5);
                String screenInfo2 = "YOU LOSE";
                canvas.drawText(screenInfo2, this.screenWidth/2, screenHeight/2, paintbrush);

                btnRestart.setxPosition(this.screenWidth/2);
                btnRestart.setyPosition((screenHeight/2+50));
                btnRestart.updateHitbox();
                        paintbrush.setStyle(Paint.Style.FILL);
                paintbrush.setStrokeWidth(8);
                canvas.drawRect(
                        btnRestart.getHitbox(),
                        paintbrush
                );
                paintbrush.setStyle(Paint.Style.STROKE);
                paintbrush.setColor(Color.argb(255, 0, 128, 0));

                String screenInfo3 = "RESTART";
                canvas.drawText(screenInfo3, this.screenWidth/2+30, screenHeight/2+200, paintbrush);
            }


            // --------------------------------
            holder.unlockCanvasAndPost(canvas);
        }

    }

    public void controlFPS() {
        try {
            gameThread.sleep(17);
        }
        catch (InterruptedException e) {

        }
    }


    // Deal with user input
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int userAction = event.getActionMasked();
        //@TODO: What should happen when person touches the screen?
        if (userAction == MotionEvent.ACTION_DOWN) {
            Log.d(TAG, "Person tapped the screen");
            event.getRawX();
            event.getRawY();


            if (status.equals("")){
                // add another bullet

                this.bullets.add(new Square(this.getContext(), 100, 700, SQUARE_WIDTH));

                // 1. calculate distance between bullet and enemy
                double a = event.getRawX() - 100;
                double b = event.getRawY() - 700;

                // d = sqrt(a^2 + b^2)

                double d = Math.sqrt((a * a) + (b * b));

                Log.d(TAG, "Distance to enemy: " + d);

                // 2. calculate xn and yn constants
                // (amount of x to move, amount of y to move)
                double xn = (a / d);
                double yn = (b / d);

                this.bullets.get((this.bullets.size()-1)).xn = xn;
                this.bullets.get((this.bullets.size()-1)).yn = yn;
            }else{
                Square btn = new Square(this.getContext(), (int)event.getRawX(), (int)event.getRawY(), 50);
                if (btn.getHitbox().intersect(this.btnRestart.getHitbox())){
                    status = "";
                    hasSparrow = false;
                    hasCat = false;
                    this.player = new Sprite(this.getContext(), 100, 700, R.drawable.player64);
                    this.sparrow = new Sprite(this.getContext(), 500, 200, R.drawable.bird64);
                    this.cat = new Sprite(this.getContext(), 1500, 700, R.drawable.cat64);
                    this.cage = new Sprite(this.getContext(), 1500, 50, R.mipmap.ic_cage);
                    this.bullets = new ArrayList<Square>();
                    sparrowDelay = 5;
                }
            }




        }
        else if (userAction == MotionEvent.ACTION_UP) {
            Log.d(TAG, "Person lifted finger");
        }

        return true;


        /*switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_DOWN:
                break;
       }
        return true;*/
    }

    // Game status - pause & resume
    public void pauseGame() {
        gameIsRunning = false;
        try {
            gameThread.join();
        }
        catch (InterruptedException e) {

        }
    }
    public void  resumeGame() {
        gameIsRunning = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

}

